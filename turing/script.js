function next_state(str) {
	return Number(str[1]);
}

function new_symbol(str) {
	return str[2];
}

function next_move(str) {
	return str[3];
}

Array.matrix = function(numrows, numcols, initial) {
    var arr = [];
    for (var i = 0; i < numrows; ++i) {
        var columns = [];
        for (var j = 0; j < numcols; ++j) {
            columns[j] = initial;
        }
        arr[i] = columns;
    }
    return arr;
}

function matrix_to_text() {
	var array = Array.matrix(5,5,"");
	for (var i = 1; i < document.getElementById("inputmatrix").rows.length; i++) {
		for (var j = 1; j < document.getElementById("inputmatrix").rows[i].cells.length; j++) {
			array[i][j] = document.getElementById("inputmatrix").rows[i].cells[j].children[0].value;
		}
	}
	return array;
}

function text_to_matrix(array) {
	for (var i = 1; i < document.getElementById("inputmatrix").rows.length; i++) {
		for (var j = 1; j < document.getElementById("inputmatrix").rows[i].cells.length; j++) {
			document.getElementById("inputmatrix").rows[i].cells[j].children[0].value = array[i*document.getElementById("inputmatrix").rows[i].cells.length+j];
		}
	}
}

function download() {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(matrix_to_text()));
	element.setAttribute('download', "dun_turing.txt");

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

var openFile = function(event) {
	var input = event.target;

	var reader = new FileReader();
	reader.onload = function(){
		var text = reader.result;
		text_to_matrix(text.split(","))
		// console.log(text.split(","));
	};
	reader.readAsText(input.files[0]);
};


var tape;
var index;
matrix = document.getElementById("inputmatrix");
var state;
var position;

function start() {
	state = 1;
	position = Number(document.getElementById("position").value);
	index = 0;

	str_tape = document.getElementById("tape").value.toUpperCase();
	tape = str_tape.split(",");

	if (!validate()) {
		alert("bad data, see example")
		return;
	}

	matrix = document.getElementById("inputmatrix");
	for (var i = 1; i < document.getElementById("inputmatrix").rows.length; i++) {
		for (var j = 1; j < document.getElementById("inputmatrix").rows[i].cells.length; j++) {
			matrix.rows[i].cells[j].children[0].value.toUpperCase();
		}
	}


	document.getElementById("execute_inp").disabled = false;
	document.getElementById("step_inp").disabled = false;

	document.getElementById("execute_limiter").disabled = true;
	document.getElementById("execute_speed").disabled = true;
	document.getElementById("inputtape").disabled = true;
	document.getElementById("tape").disabled = true;
}

function mstop(argument) {
	document.getElementById("execute_inp").disabled = true;
	document.getElementById("step_inp").disabled = true;

	document.getElementById("execute_limiter").disabled = false;
	document.getElementById("execute_speed").disabled = false;
	document.getElementById("inputtape").disabled = false;
	document.getElementById("tape").disabled = false;

	clearInterval(timerId);
}
function mstep() {
	//TODO bad S to 0, need dictinory for anover leters
	var newTapeDiv = document.createElement('div');

	cell = matrix.rows[state].cells[Number(tape[position] == "S"?-1:tape[position])+2].children[0].value;
	
	if (! /(^q\d[\dS][RLC]$)/i.test(cell)){
		newTapeDiv.innerHTML = "А ячейка q"+ state + " - " + tape[position] +" то пуста, а она мне нужна, и что вы собираетесь с этим делать шериф"
		executionlog.insertBefore(newTapeDiv, executionlog.children[0]);
		mstop();
		return;
	}
	
	state = next_state(cell);
	tape[position] = new_symbol(cell);
	
	if (next_move(cell) == "R") {
		position += 1;
	} else if(next_move(cell) == "L") {
		position -= 1;
	}
	if (position < 0) {
		position = 0;
		tape.unshift("S");
	}
	if (position >= tape.length) {
		tape.push("S");
	}

	
	newTapeDiv.innerHTML = (index++)+" "+cell+ " " + tape.slice(0, position).toString() + "[" + tape[position] + "]" + tape.slice(position+1).toString();
	
	executionlog.insertBefore(newTapeDiv, executionlog.children[0]);

	document.getElementById("execute_limiter").value -= 1;
	if(state == 0) {
		var newTapeDiv = document.createElement('div');
		newTapeDiv.innerHTML = "Висё хорошо"
		executionlog.insertBefore(newTapeDiv, executionlog.children[0]);
		mstop();
	}
}
var timerId;
function run() {
	timerId = setInterval(function() {
		mstep();
		if(document.getElementById("execute_limiter").value <= 0){
			clearInterval(timerId);
			var newTapeDiv = document.createElement('div');
			newTapeDiv.innerHTML = "Лимит закончился, а программа не выполнена"
			executionlog.insertBefore(newTapeDiv, executionlog.children[0]);
			mstop();
		}
	}, Number(document.getElementById("execute_speed").value));
}

function validate() {
	for (var i = 1; i < document.getElementById("inputmatrix").rows.length; i++) {
		for (var j = 1; j < document.getElementById("inputmatrix").rows[i].cells.length; j++) {
			if (! /((^q\d[\dS][RLC]$)|(^ *$))/i.test(document.getElementById("inputmatrix").rows[i].cells[j].children[0].value)){
				return false;
			}
		}
	}
	if (! /([S\d]\,)*([S\d])/i.test(document.getElementById("tape").value)){
		return false;
	}
	if (position >= tape.length) {
		return false;
	}
	
	return true;
}

//TODO add file of Test
document.addEventListener('DOMContentLoaded', function(){
	// console.log("load");
	mstop();
});


